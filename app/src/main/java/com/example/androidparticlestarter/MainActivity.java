package com.example.androidparticlestarter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {
    private QuestionsList questions = new QuestionsList();

    private TextView Scoreshow;
    private TextView Questionshow;
    private Button btnanswer1;
    private Button btnanswer2;




    private String answers;
    private int crntscore = 0;
    private int crntques = 0;



    // MARK: Debug info
    private final String TAG="";

    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "ekansh.sharma63@gmail.com";
    private final String PARTICLE_PASSWORD = "ekanshsharma261252";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "1a0036000e47363333343437";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();

        Scoreshow = (TextView) findViewById(R.id.scoreDisplay);
        Questionshow = (TextView) findViewById(R.id.questionDispaly);
        btnanswer1 = (Button) findViewById(R.id.button1);
        btnanswer2 = (Button) findViewById(R.id.button2);

        nextQuestion();

        btnanswer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnanswer1.getText() == answers) {
                    crntscore = crntscore + 1;
                    nextQuestion();
                    Toast.makeText(MainActivity.this, "Correct Answer", Toast.LENGTH_SHORT).show();
                    photonResponse("2");

                } else {
                    Toast.makeText(MainActivity.this, "Wrong Answer", Toast.LENGTH_SHORT).show();
                    nextQuestion();
                    photonResponse("1");
                }
            }
        });
        btnanswer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnanswer2.getText() == answers) {
                    crntscore = crntscore + 1;
                    nextQuestion();
                    Toast.makeText(MainActivity.this, "Correct Answer", Toast.LENGTH_SHORT).show();
                    photonResponse("2");
                } else {
                    Toast.makeText(MainActivity.this, "Wrong Answer", Toast.LENGTH_SHORT).show();
                    nextQuestion();
                    photonResponse("1");
                }
            }
        });


    }


    /**
     * Custom function to connect to the Particle Cloud and get the device
     */
    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }
    public void photonResponse(String ch) {


        String choiceS = ch;

        // 1. check if device has a value
        // - if device is null, then quit!
        if (this.mDevice == null) {
            Log.d(TAG, "ERROR: No device found.");
            return;
        }





       // Log.d(TAG, "User wants to turn on " + numLights + " lights");

        // 3. Send the value to the Particle
        //  - if success, display an success message
        //  - if error, display failure message

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                // put your logic here to talk to the particle
                // --------------------------------------------

                // what functions are "public" on the particle?
                Log.d(TAG, "Availble functions: " + mDevice.getFunctions());

                // call the "lights" function on the particle

                List<String> functionParameters = new ArrayList<String>();
               functionParameters.add(choiceS);
                try {
                    mDevice.callFunction("choice", functionParameters);
                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }

    private void nextQuestion(){
        Questionshow.setText(questions.getQuestion(crntques));
        btnanswer1.setText(questions.getChoice1(crntques));
        btnanswer2.setText(questions.getChoice2(crntques));


        answers = questions.getCorrectAnswer(crntques);

        Scoreshow.setText("Score: "+crntscore+"/10");
        crntques++;


        if (crntques == 11 ) {
            {
                if (crntscore >= 0 && crntscore <=3) {

                    AlertDialog alertDialogBuilder = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialogBuilder.setTitle("Please try again!");
                    alertDialogBuilder.setMessage("Your score is " + crntscore + "/10");
                    alertDialogBuilder.setButton(DialogInterface.BUTTON_POSITIVE, "Restart", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        }
                    });
                    alertDialogBuilder.setButton(DialogInterface.BUTTON_NEGATIVE, "Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialogBuilder.show();

                } else if (crntscore >= 4 && crntscore <=6) {
                    AlertDialog alertDialogBuilder = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialogBuilder.setTitle("Good job!");
                    alertDialogBuilder.setMessage("Your Score is " + crntscore + "/10");
                    alertDialogBuilder.setButton(DialogInterface.BUTTON_POSITIVE, "Restart", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        }
                    });
                    alertDialogBuilder.setButton(DialogInterface.BUTTON_NEGATIVE, "Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialogBuilder.show();
                } else if (crntscore >= 7 && crntscore <=9) {
                    AlertDialog alertDialogBuilder = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialogBuilder.setTitle("Excellent Work!");
                    alertDialogBuilder.setMessage("Your Score is " + crntscore + "/10");
                    alertDialogBuilder.setButton(DialogInterface.BUTTON_POSITIVE, "Restart", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        }
                    });
                    alertDialogBuilder.setButton(DialogInterface.BUTTON_NEGATIVE, "Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialogBuilder.show();

                } else if (crntscore == 10) {
                    AlertDialog alertDialogBuilder = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialogBuilder.setTitle("You are a genius!");
                    alertDialogBuilder.setMessage("Your Score is " + crntscore + "/10");
                    alertDialogBuilder.setButton(DialogInterface.BUTTON_POSITIVE, "Restart", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        }
                    });
                    alertDialogBuilder.setButton(DialogInterface.BUTTON_NEGATIVE, "Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialogBuilder.show();
                }
            }


        }
    }


    private void newScore(int point) {
        Scoreshow.setText("" + crntscore);
    }
}




